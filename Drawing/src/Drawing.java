import javax.swing.JApplet;
import java.awt.Graphics;
public class Drawing extends JApplet
{

	public void paint(Graphics canvas)
	{
		canvas.drawOval(50, 50, 100, 100);
		canvas.fillOval(75, 75, 50, 50);
		canvas.drawArc(25, -50, 150, 100, 180, 180);
		canvas.drawArc(150, 25, 100, 150, 90, 180);
		canvas.drawArc(-50, 25, 100, 150, 90, -180);
		canvas.drawArc(25, 150, 150, 100, 0, 180);
	}

}

